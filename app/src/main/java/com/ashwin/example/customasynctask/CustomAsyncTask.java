package com.ashwin.example.customasynctask;

import android.os.Binder;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.annotation.MainThread;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.util.Log;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Ashwin on 11-11-2017.
 */

public abstract class CustomAsyncTask<Params, Progress, Result> {

    private static final String TAG = CustomAsyncTask.class.getSimpleName();

    // Status
    public enum Status {
        PENDING,
        RUNNING,
        FINISHED
    }

    // Thread pool
    private static final int CORE_POOL_SIZE = 5;
    private static final int MAX_THREAD_POOL_SIZE = 10;
    private static final int TIMEOUT_SECONDS = 30;
    private static final BlockingQueue<Runnable> sPoolWorkQueue = new LinkedBlockingQueue<Runnable>(128);

    private static final ThreadFactory mThreadFactory = new ThreadFactory() {
        // Initialize thead number
        private int mCount = 1;

        public Thread newThread(Runnable r) {
            // Increment thread number
            mCount++;

            // Create and return new thread
            return new Thread(r, "CustomAsyncTask #" + mCount);
        }
    };

    // Thread pool executor executes tasks sequentially
    public static final Executor mThreadPoolExecutor = new ThreadPoolExecutor(
            CORE_POOL_SIZE, MAX_THREAD_POOL_SIZE, TIMEOUT_SECONDS, TimeUnit.SECONDS, sPoolWorkQueue, mThreadFactory);

    private static class SerialExecutor implements Executor {
        final Queue<Runnable> mRunnables = new PriorityQueue<>();
        Runnable mCurrentRunnable;

        public synchronized void execute(final Runnable r) {
            // Add runnable to queue
            mRunnables.offer(new Runnable() {
                public void run() {
                    try {
                        // Execute the current task
                        r.run();
                    } finally {
                        // Execute the next task in queue
                        executeNext();
                    }
                }
            });

            // Execute the next if current task is null
            if (mCurrentRunnable == null) {
                executeNext();
            }
        }

        // Execute the next runnable
        protected synchronized void executeNext() {
            if ((mCurrentRunnable = mRunnables.poll()) != null) {
                mThreadPoolExecutor.execute(mCurrentRunnable);
            }
        }
    }

    private static final int POST_RESULT_MESSAGE = 200;
    private Status mStatus = Status.PENDING;
    private boolean mCancelled = false;
    private static Executor mExecutor;
    private final Handler mHandler;
    private static TaskHandler mTaskHandler;
    private final BackgroundRunnable<Params, Result> mBackgroundRunnable;
    private final FutureTask<Result> mFutureTask;

    public CustomAsyncTask() {
        this((Looper) null);
    }

    public CustomAsyncTask(@Nullable Looper callbackLooper) {
        // Get the thread
        mHandler = callbackLooper == null || callbackLooper == Looper.getMainLooper() ? getMainHandler() : new Handler(callbackLooper);

        // Background runnable will be added to thread pool queue and run on execute call
        mBackgroundRunnable = new BackgroundRunnable<Params, Result>() {
            @Override
            public Result call() throws Exception {
                Result result = null;
                try {
                    // Run in the background
                    Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                    result = onExecute(mParams);
                    Binder.flushPendingCommands();
                } catch (Throwable tr) {
                    mCancelled = true;
                    throw tr;
                }
                return result;
            }
        };

        // Future task stores the result
        mFutureTask = new FutureTask<Result>(mBackgroundRunnable) {
            @Override
            protected void done() {
                try {
                    Log.d(TAG, "CustomAsyncTask() : futuretask.get() : " + get());
                    postResult(get());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        // Executor is initialized as serial executor
        mExecutor = new SerialExecutor();
    }

    private static abstract class BackgroundRunnable<Params, Result> implements Callable<Result> {
        Params[] mParams;
    }

    private static class CustomAsyncTaskResult<Data> {
        CustomAsyncTask mCustomAsyncTask;
        Data[] mResult;

        CustomAsyncTaskResult(CustomAsyncTask customeAsyncTask, Data... result) {
            mCustomAsyncTask = customeAsyncTask;
            mResult = result;
            Log.d(TAG, "CustomAsyncTaskResult : " + mResult[0]);
        }
    }

    private static class TaskHandler extends Handler {
        public TaskHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            CustomAsyncTaskResult<?> customAsyncTaskResult = (CustomAsyncTaskResult<?>) msg.obj;
            switch (msg.what) {
                case POST_RESULT_MESSAGE:
                    Log.d(TAG, "handleMessage() : data : " + customAsyncTaskResult.mResult[0]);
                    customAsyncTaskResult.mCustomAsyncTask.finish(customAsyncTaskResult.mResult[0]);
                    break;
            }
        }
    }

    private static Handler getMainHandler() {
        synchronized (CustomAsyncTask.class) {
            if (mTaskHandler == null) {
                mTaskHandler = new TaskHandler(Looper.getMainLooper());
            }
            return mTaskHandler;
        }
    }

    @MainThread
    public Status getStatus() {
        return mStatus;
    }

    @MainThread
    protected void onCancelled() {
    }

    @MainThread
    protected abstract void beforeExecute();

    @WorkerThread
    protected abstract Result onExecute(Params... params);

    @MainThread
    protected abstract void afterExecute(Result result);

    @MainThread
    public CustomAsyncTask<Params, Progress, Result> execute(final Params... params) {
        // Check if task is not already running
        if (mStatus != Status.PENDING) {
            return null;
        }

        // Update status
        mStatus = Status.RUNNING;

        // onPreExecute
        beforeExecute();

        // doInBackground
        mBackgroundRunnable.mParams = params;

        // Execute
        mExecutor.execute(mFutureTask);

        return this;
    }

    private Result postResult(Result result) {
        Log.d(TAG, "postResult() : " + result);

        Message message = mHandler.obtainMessage(POST_RESULT_MESSAGE, new CustomAsyncTaskResult<Result>(this, result));
        message.sendToTarget();
        return result;
    }

    private void finish(Result result) {
        if (mCancelled) {
            onCancelled();
        } else {
            afterExecute(result);
        }
        mStatus = Status.FINISHED;
    }

}
