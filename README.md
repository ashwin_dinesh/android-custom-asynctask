# Custom Android AsyncTask

A custom AsyncTask built from scratch.

### Sample usage

```gradle
public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(TAG, "onCreate() : current thread id : " + Thread.currentThread().getId());
        mTextView = (TextView) findViewById(R.id.textView);
        startCustomAsyncTask();
    }

    private void startCustomAsyncTask() {
        new CustomAsyncTask<String, Void, String>() {
            @Override
            protected void beforeExecute() {
                // Runs on ui thread
                Long threadId = Thread.currentThread().getId();
                Log.d(TAG, "beforeExecute() : current thread id : " + threadId);
                mTextView.setText("Loading...");
            }

            @Override
            protected String onExecute(String... params) {
                // Goes to background thread
                Long threadId = Thread.currentThread().getId();
                Log.d(TAG, "onExecute() : current thread id : " + threadId);
                try {
                    // Sleep for 5 seconds
                    Thread.sleep(5000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String result = "Custom async task success";
                return result;
            }

            @Override
            protected void afterExecute(String str) {
                // Back on ui thread
                Long threadId = Thread.currentThread().getId();
                Log.d(TAG, "afterExecute() : current thread id : " + threadId);
                mTextView.setText(str);
            }
        }.execute();
    }
}
```